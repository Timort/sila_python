# Generated by sila2.code_generator; sila2.__version__: 0.9.2
# -----
# This class does not do anything useful at runtime. Its only purpose is to provide type annotations.
# Since sphinx does not support .pyi files (yet?), so this is a .py file.
# -----

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:

    from typing import Iterable, Optional

    from observablepropertytest_types import SetValue_Responses

    from sila2.client import ClientMetadataInstance, ClientObservableProperty


class ObservablePropertyTestClient:
    """
    This is a test feature to test observable properties.
    """

    FixedValue: ClientObservableProperty[int]
    """
    Always returns 42 and never changes.
    """

    Alternating: ClientObservableProperty[bool]
    """
    Switches every second between true and false
    """

    Editable: ClientObservableProperty[int]
    """
    Can be set through SetValue command
    """

    def SetValue(
        self, Value: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetValue_Responses:
        """
        Changes the value of Editable
        """
        ...
