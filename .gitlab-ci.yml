# python 3.7 is the minimum supported version for this library
image: python:3.7-slim

cache:
  paths:
    - .cache/pip

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

before_script:
  - python -m pip install --upgrade pip setuptools

build:
  stage: build
  script:
    - python -m pip install -e .[codegen]  # install code generator
    - python generate-feature-submodule.py  # generate sila2.features submodule code
    - python -m pip install build
    - python -m build -sw -o dist/ .  # build full package
    - python -m build -sw -o dist/ ./example_server/  # build example server
    - python -m build -sw -o dist/ ./compatibility_test_server/  # build compatibility test server
  artifacts:
    paths:
      - dist/

codequality:
  stage: test
  script:
    - python -m pip install black isort pyproject-flake8 'flake8<5.0.0'
    - python -m isort --check-only .
    - python -m black --check --fast .
    - python -m pflake8 .

# install to test if everything runs without resources from the source directory
test:
  stage: test
  script:
    - python -m pip install $(ls dist/sila2-*.whl)[codegen,test,xmlschema,jsonschema]
    - python -m pip install dist/sila2_example_server-*.whl
    - python -m pytest tests/

# for coverage, tests need to execute the files in src/
coverage:
  stage: test
  coverage: '/TOTAL.* (\d+\.\d+)%/'
  script:
    - python -m pip install -e .[codegen,test,xmlschema,jsonschema]
    - python generate-feature-submodule.py
    - python -m pip install -e example_server/
    - python -m pytest --cov=src --cov-config=pyproject.toml tests/
    - python -m coverage xml
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

compatibility-client-test:
  image: python:3.9
  stage: test
  allow_failure: true
  script:
    # install interop server/client package
    - python -m pip install dist/sila2-*.whl
    - python -m pip install dist/sila2_python_compatibility_test_server-*.whl
    # download and install test program
    - wget https://gitlab.com/SiLA2/sila_interoperability/-/jobs/artifacts/main/raw/sila2_interop_communication_tester-0.0.0-py3-none-any.whl?job=build-python -O sila2_interop_communication_tester-0.0.0-py3-none-any.whl
    - pip install sila2_interop_communication_tester-0.0.0-py3-none-any.whl
    # run test server
    - python -m sila2_interop_communication_tester.test_server --report-file client-test-report.xml --testsuite-name "SiLA Python Dynamic Client" > server.log 2>&1 & server_pid=$!
    - sleep 5
    # run client
    - python compatibility_test_client/main.py
    # interrupt server, process will then generate the report
    - kill -INT $server_pid
    - wait $server_pid
  artifacts:
    when: always
    paths:
      - server.log
      - client-test-report.xml

compatibility-server-test:
  image: python:3.9
  stage: test
  allow_failure: true
  script:
    # install sila python compatibility test server, using the current sila_python version
    - python -m pip install dist/sila2-*.whl
    - python -m pip install dist/sila2_python_compatibility_test_server-*.whl
    # install test tool
    - wget https://gitlab.com/SiLA2/sila_interoperability/-/jobs/artifacts/main/raw/sila2_interop_communication_tester-0.0.0-py3-none-any.whl?job=build-python -O sila2_interop_communication_tester-0.0.0-py3-none-any.whl
    - pip install sila2_interop_communication_tester-0.0.0-py3-none-any.whl
    # start server
    - python -m sila2_python_compatibility_test_server --insecure --port 50052 --ip-address 127.0.0.1 --debug >server.log 2>&1 & server_pid=$!
    - sleep 5
    # run test client, will generate report
    - python -m sila2_interop_communication_tester.test_client --report-file server-test-report.xml --testsuite-name "SiLA Python"
    # stop server
    - kill $server_pid
    - wait $server_pid
  artifacts:
    when: always
    paths:
      - server.log
      - server-test-report.xml

pages:
  stage: deploy
  only:
    - master
  script:
    - python -m pip install $(ls dist/sila2-*.whl)[docs,codegen]
    - python docs/make-docs.py html
    - mv ./docs/_build/html ./public
  artifacts:
    paths:
      - public/
