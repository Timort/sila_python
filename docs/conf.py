# Configuration file for the Sphinx documentation builder
import sys
from glob import glob
from os.path import dirname, join, relpath

import sila2

project = sila2.__name__
author = "Niklas Mertsch"
copyright = f"2022, {author}"
release = sila2.__version__
language = "en"

exclude_patterns = ["_build"]

html_theme = "nature"
html_static_path = ["_static"]
static_dir = join(dirname(__file__), "_static")
html_css_files = [relpath(p, static_dir) for p in glob(join(static_dir, "css", "*.css"))]
html_js_files = [relpath(p, static_dir) for p in glob(join(static_dir, "js", "*.js"))]
html_favicon = "_static/favicon.ico"

pygments_style = "sphinx"

autoclass_content = "both"
autodoc_default_options = {
    "inherited-members": True,  # when set to False, some non-inherited members won't be displayed (no clue why...)
    "exclude-members": ", ".join(
        [
            # (named) tuples
            "count",
            "index",
            # exceptions
            "with_traceback",
        ]
    ),
}

sys.path.append("extensions")
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinxcontrib.napoleon",
    "sphinxcontrib.runcmd",
    "intersphinx_typing_fix",
]
autodoc_member_order = "bysource"
intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
}
napoleon_use_rtype = False
