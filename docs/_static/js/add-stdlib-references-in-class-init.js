// For some reason, stdlib types are not converted to references in class __init__ methods
$(document).ready(function () {
    function createUrlNode(name, url) {
        const a = document.createElement("a")
        a.setAttribute("class", "reference external")
        a.setAttribute("href", url)
        const span = document.createElement("span")
        span.innerHTML = name
        a.appendChild(span)
        return a
    }

    $('dl[class="py class"] dt[class="sig sig-object py"] em[class="sig-param"] span[class="n"]').each(function () {
        let node = document.evaluate('./preceding-sibling::span[@class="w"]', this).iterateNext()
        if (node != null) {
            $(this).children('span[class="pre"]').each(function () {
                switch (this.innerHTML) {
                    case "UUID":
                        this.replaceWith(createUrlNode("UUID", "https://docs.python.org/3/library/uuid.html#uuid.UUID"))
                        break
                    case "timedelta":
                        this.replaceWith(createUrlNode("timedelta", "https://docs.python.org/3/library/datetime.html#datetime.timedelta"))
                        break
                }
            })
        }
    })
})
