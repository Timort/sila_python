Add features to existing SiLA Server/Client package
===================================================

To add new SiLA features to an existing package, run ``sila2-codegen add-features Feature1.sila.xml Feature2.sila.xml ...`` from the package directory (or specify the package directory with ``--package-dir``). See also :doc:`/content/code_generator/add_features`.

This will add and update the required files in the ``generated`` directory, and add ``..._impl.py`` files in the ``feature_implementations`` directory. It will NOT add the feature to the SiLA Server class in ``server.py``.

To manually add your feature implementation to the server, use the method :py:func:`~sila2.server.SilaServer.set_feature_implementation` of the ``Server`` class:

.. code-block:: python

    # in server.py
    from .generated.mynewfeature import MyNewFeatureFeature
    from .feature_implementations.mynewfeature_impl import MyNewFeatureImpl

    class Server(SilaServer):
        def __init__(...):
            ...

            # add Impl as feature implementation
            self.set_feature_implementation(MyNewFeatureFeature, MyNewFeatureImpl(self))
