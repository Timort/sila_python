Class ClientMetadata
====================

.. autoclass:: sila2.client.ClientMetadata
    :special-members: __call__

Class ClientMetadataInstance
============================

.. autoclass:: sila2.client.ClientMetadataInstance
