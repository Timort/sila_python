import functools
import os

import pytest
from sila2_example_server import Server

from sila2.framework.utils import temporarily_add_to_path


def run_example_client_script(func):
    ca_filename = "ca.pem"

    @functools.wraps(func)
    @pytest.mark.filterwarnings("ignore")
    def wrapper(*args, **kwargs):
        server = Server()
        try:
            server.start("127.0.0.1", 50052)
            with open(ca_filename, "wb") as ca_fp:
                ca_fp.write(server.generated_ca)

            with temporarily_add_to_path(os.path.join(os.path.dirname(__file__), "..", "example_client_scripts")):
                return func(*args, **kwargs)
        finally:
            server.stop()
            os.remove(ca_filename)

    return wrapper


@run_example_client_script
def test_error_handling():
    import error_handling

    error_handling.main()


@run_example_client_script
def test_metadata():
    import metadata

    metadata.main()


@run_example_client_script
def test_observable_command():
    import observable_command

    observable_command.main()


@run_example_client_script
def test_observable_property():
    import observable_property

    observable_property.main()


@run_example_client_script
def test_unobservable_command():
    import unobservable_command

    unobservable_command.main()


@run_example_client_script
def test_unobservable_property():
    import unobservable_property

    unobservable_property.main()
