import os
import random
import string
import subprocess
import sys
import tempfile
import time
import uuid
from contextlib import contextmanager
from os.path import join

import pytest
from pytest import fail

from sila2.client import ClientObservableCommandInstance, SilaClient
from sila2.features.silaservice import SiLAServiceFeature
from sila2.framework import DefinedExecutionError, InvalidMetadata, NoMetadataAllowed, UndefinedExecutionError
from tests.utils import generate_port, get_fdl_path


def generate_package_name() -> str:
    return "".join(random.choices(string.ascii_lowercase + string.ascii_uppercase, k=50))


@contextmanager
def install_package_and_run_client(package_name, package_dir) -> SilaClient:
    # install package
    install_process = subprocess.Popen(
        [
            sys.executable,
            "-m",
            "pip",
            "install",
            package_dir,
            "--no-build-isolation",  # use local setuptools installation for offline usage
        ]
    )
    install_process.wait(timeout=30)

    try:
        port = generate_port()

        # start server
        server_process = subprocess.Popen(
            [sys.executable, "-m", package_name, "-p", str(port), "--insecure"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
        )
        try:
            # wait for server startup
            time.sleep(5)

            # import and run client
            exec(f"from {package_name} import Client", globals())
            yield Client("127.0.0.1", port, insecure=True)  # noqa: F821
        finally:
            # shutdown server
            server_process.terminate()
    finally:
        # uninstall package
        subprocess.Popen([sys.executable, "-m", "pip", "uninstall", "-y", package_name]).wait(timeout=30)


def test_empty_server():
    with tempfile.TemporaryDirectory() as tmp_dir:
        package_name = generate_package_name()

        # generate package
        codegen_proc = subprocess.Popen(["sila2-codegen", "new-package", "-o", tmp_dir, "-n", package_name])
        assert codegen_proc.wait(timeout=30) == 0

        # test client
        with install_package_and_run_client(package_name, tmp_dir) as client:
            assert client.SiLAService.ImplementedFeatures.get() == [SiLAServiceFeature.fully_qualified_identifier]


def test_installation_with_sibling_directory():
    # tests if the generated pyproject.toml tells setuptools to install only this package
    with tempfile.TemporaryDirectory() as tmp_dir:
        package_name = generate_package_name()

        # generate package
        codegen_proc = subprocess.Popen(["sila2-codegen", "new-package", "-o", tmp_dir, "-n", package_name])
        assert codegen_proc.wait(timeout=30) == 0

        # generate sibling
        sibling_dir_name = "sibling_dir"
        os.makedirs(join(tmp_dir, sibling_dir_name))  # generate directory
        with open(join(tmp_dir, sibling_dir_name, "__init__.py"), "w"):
            pass  # generate __init__.py file to generate a namespace that should be ignored

        # test client
        with install_package_and_run_client(package_name, tmp_dir):
            try:
                import sibling_dir  # noqa: F401

                fail("Sibling package should not be installed")
            except ImportError:
                pass
            finally:
                if sibling_dir_name in dir():
                    subprocess.Popen([sys.executable, "-m", "pip", "uninstall", "-y", sibling_dir_name]).wait(
                        timeout=30
                    )


def test_server_with_empty_greetingprovider():
    with tempfile.TemporaryDirectory() as tmp_dir:
        package_name = generate_package_name()

        # generate package
        codegen_proc = subprocess.Popen(
            ["sila2-codegen", "new-package", "-o", tmp_dir, "-n", package_name, get_fdl_path("GreetingProvider")]
        )
        assert codegen_proc.wait(timeout=30) == 0

        # test client
        with install_package_and_run_client(package_name, tmp_dir) as client:
            assert len(client.SiLAService.ImplementedFeatures.get()) == 2

            with pytest.raises(UndefinedExecutionError):
                client.GreetingProvider.SayHello("World")
            with pytest.raises(UndefinedExecutionError):
                client.GreetingProvider.StartYear.get()


def test_server_with_implemented_greetingprovider():
    with tempfile.TemporaryDirectory() as tmp_dir:
        package_name = generate_package_name()

        # generate package
        codegen_proc = subprocess.Popen(
            ["sila2-codegen", "new-package", "-o", tmp_dir, "-n", package_name, get_fdl_path("GreetingProvider")]
        )
        assert codegen_proc.wait(timeout=30) == 0

        # add implementation
        impl_file = join(tmp_dir, package_name, "feature_implementations", "greetingprovider_impl.py")
        content = open(impl_file).read()
        content = content.replace("raise NotImplementedError  # TODO", "return 2000", 1)
        content = content.replace("raise NotImplementedError  # TODO", "return f'Hello {Name}'", 1)
        with open(impl_file, "w", encoding="utf-8") as fp:
            fp.write(content)

        # test client
        with install_package_and_run_client(package_name, tmp_dir) as client:
            assert len(client.SiLAService.ImplementedFeatures.get()) == 2
            assert client.GreetingProvider.SayHello("World") == ("Hello World",)
            assert client.GreetingProvider.StartYear.get() == 2000


def test_add_features():
    with tempfile.TemporaryDirectory() as tmp_dir:
        package_name = generate_package_name()

        # generate package
        codegen_proc = subprocess.Popen(["sila2-codegen", "new-package", "-o", tmp_dir, "-n", package_name])
        assert codegen_proc.wait(timeout=30) == 0

        # add feature to package
        codegen_proc = subprocess.Popen(
            ["sila2-codegen", "add-features", "--package-directory", tmp_dir, get_fdl_path("ObservableCommand")]
        )
        assert codegen_proc.wait(timeout=30) == 0

        # add feature to server
        server_file = join(tmp_dir, package_name, "server.py")
        lines = open(server_file).readlines()
        lines.insert(1, "from .generated.observablecommand import ObservableCommandFeature\n")
        lines.insert(1, "from .feature_implementations.observablecommand_impl import ObservableCommandImpl\n")
        lines.append("        self.set_feature_implementation(ObservableCommandFeature, ObservableCommandImpl(self))\n")
        with open(server_file, "w", encoding="utf-8") as fp:
            fp.writelines(lines)

        # test client
        with install_package_and_run_client(package_name, tmp_dir) as client:
            assert len(client.SiLAService.ImplementedFeatures.get()) == 2

            with pytest.raises(UndefinedExecutionError):
                instance: ClientObservableCommandInstance = client.ObservableCommand.SendCharacters("Teststring")
                time.sleep(2)
                instance.get_responses()

        # cannot add feature twice
        codegen_proc = subprocess.Popen(
            ["sila2-codegen", "add-features", "--package-dir", tmp_dir, get_fdl_path("ObservableCommand")]
        )
        assert codegen_proc.wait(timeout=30) != 0


def test_lock_controller():
    with tempfile.TemporaryDirectory() as tmp_dir:
        package_name = generate_package_name()

        # generate package with LockController
        codegen_proc = subprocess.Popen(
            [
                "sila2-codegen",
                "new-package",
                "-o",
                tmp_dir,
                "-n",
                package_name,
                "--lock-controller",
                get_fdl_path("GreetingProvider"),
            ]
        )
        assert codegen_proc.wait(timeout=30) == 0

        # test client
        with install_package_and_run_client(package_name, tmp_dir) as client:
            assert len(client.SiLAService.ImplementedFeatures.get()) == 3

            assert client.LockController.IsLocked.get() is False

            with pytest.raises(InvalidMetadata):
                client.GreetingProvider.SayHello("World")

            lock_token = "my-lock-token"
            client.LockController.LockServer(lock_token, 60)  # lock for a minute
            assert client.LockController.IsLocked.get() is True

            with pytest.raises(InvalidMetadata):
                client.GreetingProvider.SayHello("World")

            with pytest.raises(DefinedExecutionError) as ex:
                client.GreetingProvider.SayHello("World", metadata=[client.LockController.LockIdentifier("abc")])
            assert ex.value.__class__.__name__ == "InvalidLockIdentifier"

            with pytest.raises(UndefinedExecutionError):
                client.GreetingProvider.SayHello("World", metadata=[client.LockController.LockIdentifier(lock_token)])

            # locking should not affect SiLAService
            assert len(client.SiLAService.ImplementedFeatures.get()) == 3

            # sending metadata with SiLAService requests is illegal
            with pytest.raises(NoMetadataAllowed):
                client.SiLAService.ImplementedFeatures.get(metadata=[client.LockController.LockIdentifier(lock_token)])

            with pytest.raises(DefinedExecutionError) as ex:
                client.LockController.LockServer("abc", 60)
            assert ex.value.__class__.__name__ == "ServerAlreadyLocked"

            client.LockController.UnlockServer(lock_token)
            assert client.LockController.IsLocked.get() is False

            with pytest.raises(InvalidMetadata):
                client.GreetingProvider.SayHello("World")

            with pytest.raises(DefinedExecutionError) as ex:
                client.LockController.UnlockServer("abc")
            assert ex.value.__class__.__name__ == "ServerNotLocked"


def test_auth_features():
    with tempfile.TemporaryDirectory() as tmp_dir:
        package_name = generate_package_name()

        # generate package with Auth features
        codegen_proc = subprocess.Popen(
            [
                "sila2-codegen",
                "new-package",
                "-o",
                tmp_dir,
                "-n",
                package_name,
                "--auth-features",
                get_fdl_path("GreetingProvider"),
            ]
        )
        assert codegen_proc.wait(timeout=30) == 0

        # test client
        with install_package_and_run_client(package_name, tmp_dir) as client:
            assert len(client.SiLAService.ImplementedFeatures.get()) == 5
            server_uuid = client.SiLAService.ServerUUID.get()
            greetingprovider_fqi = client.GreetingProvider.fully_qualified_identifier

            # without login
            with pytest.raises(InvalidMetadata):
                client.GreetingProvider.SayHello("World")
            with pytest.raises(DefinedExecutionError) as ex:
                client.GreetingProvider.SayHello("World", metadata=[client.AuthorizationService.AccessToken("abc")])
            assert ex.value.__class__.__name__ == "InvalidAccessToken"

            # login
            token = client.AuthenticationService.Login(
                "admin", "admin", server_uuid, [greetingprovider_fqi]
            ).AccessToken

            with pytest.raises(UndefinedExecutionError):
                client.GreetingProvider.SayHello("World", metadata=[client.AuthorizationService.AccessToken(token)])

            (remaining_seconds,) = client.AuthorizationProviderService.Verify(token, server_uuid, greetingprovider_fqi)
            assert isinstance(remaining_seconds, int)
            assert remaining_seconds > 0

            with pytest.raises(DefinedExecutionError) as ex:
                client.AuthorizationProviderService.Verify(str(uuid.uuid4()), server_uuid, greetingprovider_fqi)
            assert ex.value.__class__.__name__ == "AuthorizationFailed"

            with pytest.raises(DefinedExecutionError) as ex:
                client.AuthorizationProviderService.Verify("abc", server_uuid, greetingprovider_fqi)
            assert ex.value.__class__.__name__ == "InvalidAccessToken"

            # logout
            client.AuthenticationService.Logout(token)
            with pytest.raises(DefinedExecutionError) as ex:
                client.GreetingProvider.SayHello("World", metadata=[client.AuthorizationService.AccessToken(token)])
            assert ex.value.__class__.__name__ == "InvalidAccessToken"
