import pytest
from lxml.etree import XMLSyntaxError

from sila2.framework import Metadata
from sila2.framework.command.observable_command import ObservableCommand
from sila2.framework.command.unobservable_command import UnobservableCommand
from sila2.framework.feature import Feature
from sila2.framework.property.observable_property import ObservableProperty
from sila2.framework.property.unobservable_property import UnobservableProperty
from tests.utils import get_feature_definition_str


@pytest.fixture
def empty_feature() -> Feature:
    return Feature(get_feature_definition_str("EmptyFeature"))


def test_get_feature_definition_str():
    assert isinstance(get_feature_definition_str("EmptyFeature"), str)


def test_string_attributes(empty_feature):
    assert empty_feature._identifier == "EmptyFeature"
    assert empty_feature._display_name == "Empty Feature"
    assert empty_feature._description == "An empty feature"
    assert empty_feature.fully_qualified_identifier == "de.unigoettingen/tests/EmptyFeature/v1"
    assert empty_feature._sila2_version == "1.0"
    assert empty_feature._feature_version == "1.0"
    assert empty_feature._maturity_level == "Draft"
    assert empty_feature._locale == "en-us"
    assert empty_feature._originator == "de.unigoettingen"
    assert empty_feature._category == "tests"


def test_pb2_modules(empty_feature):
    assert empty_feature._pb2_module.__name__ == "EmptyFeature_pb2"
    assert empty_feature._grpc_module.__name__ == "EmptyFeature_pb2_grpc"


def test_fail_schema_validation():
    with pytest.raises(XMLSyntaxError):
        _ = Feature(get_feature_definition_str("FeatureWithoutDescription"))


def test_fail_fdl_validation():
    with pytest.raises(ValueError):
        _ = Feature(get_feature_definition_str("ListOfLists"))


def test_read_data_type_definitions_first():
    # fails if commands are read first and can't find the defined data types
    _ = Feature(get_feature_definition_str("DataTypeDefinitionInCommand"))


def test_reverse_order_data_type_definitions():
    # fails if data type definitions are parsed from top to bottom
    _ = Feature(get_feature_definition_str("ReverseOrderDataTypes"))


def test_empty_display_name_and_description():
    f = Feature(get_feature_definition_str("EmptyDisplayNameAndDescription"))
    assert f._display_name == ""
    assert f._description == ""


def test_getitem():
    f = Feature(get_feature_definition_str("GreetingProvider"))
    assert isinstance(f["SayHello"], UnobservableCommand)
    assert isinstance(f["StartYear"], UnobservableProperty)

    f = Feature(get_feature_definition_str("Metadata"))
    assert isinstance(f["Meta"], Metadata)

    f = Feature(get_feature_definition_str("ObservableCommand"))
    assert isinstance(f["SendCharacters"], ObservableCommand)

    f = Feature(get_feature_definition_str("ObservableProperty"))
    assert isinstance(f["TestProperty"], ObservableProperty)

    with pytest.raises(KeyError):
        _ = f["ThisDoesNotExist"]
