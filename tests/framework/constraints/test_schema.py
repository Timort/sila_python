import json
from unittest.mock import patch

import pytest

from sila2.framework.constraints.schema import Schema, SchemaSourceType, SchemaType
from tests.utils import resources_dir as test_resource_dir


def test_invalid_schema():
    with pytest.raises(Exception):
        _ = Schema(SchemaType.Json, SchemaSourceType.Inline, "abc")
    with pytest.raises(Exception):
        _ = Schema(SchemaType.Xml, SchemaSourceType.Inline, "abc")


# mock urllib.request.urlopen in xmlschema.resources to use local schema files
@patch(
    "xmlschema.resources.urlopen",
    lambda url, **kwargs: open(url.replace("https://some-arbitrary-url.xyz/", f"{test_resource_dir}/xsd/"), "rb"),
)
def test_xml():
    for constraint in (
        Schema(SchemaType.Xml, SchemaSourceType.Url, "https://some-arbitrary-url.xyz/person.xsd"),
        Schema(
            SchemaType.Xml,
            SchemaSourceType.Inline,
            """<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xs:element name="person">
    <xs:complexType>
        <xs:sequence>
            <xs:element name="name" type="xs:string"/>
            <xs:element name="age" type="xs:integer"/>
        </xs:sequence>
    </xs:complexType>
</xs:element>
</xs:schema>""",
        ),
    ):
        assert constraint.validate('<?xml version="1.0"?><person><name>Jerry</name><age>123</age></person>'.strip())
        assert not constraint.validate('<?xml version="1.0"?><person><name>Jerry</name><age>abc</age></person>'.strip())

        assert constraint.validate(
            '<?xml version="1.0"?><person><name>Jerry</name><age>123</age></person>'.strip().encode("utf-8")
        )
        assert not constraint.validate(
            '<?xml version="1.0"?><person><name>Jerry</name><age>abc</age></person>'.strip().encode("utf-8")
        )


# mock urllib.request.urlopen in schema constraint to use local files
@patch(
    f"{Schema.__module__}.urlopen",
    lambda url, **kwargs: open(url.replace("https://json-schema.org", f"{test_resource_dir}/jsonschema/"), "rb"),
)
def test_url_json():
    schema_url = "https://json-schema.org/draft-04/schema"
    constraint = Schema(SchemaType.Json, SchemaSourceType.Url, schema_url)
    assert constraint.validate(
        json.dumps(
            {
                "type": "object",
                "properties": {"name": {"type": "string"}, "age": {"type": "number"}},
            }
        )
    )

    assert repr(constraint) == "Schema(Json, Url, 'https://json-schema.org/draft-04/schema')"

    assert not constraint.validate(json.dumps([1, 2, 3]))


def test_inline_json():
    schema = {
        "type": "object",
        "properties": {"name": {"type": "string"}, "age": {"type": "number"}},
    }
    constraint = Schema(SchemaType.Json, SchemaSourceType.Inline, json.dumps(schema))
    assert constraint.validate(json.dumps({"name": "Jerry", "age": 5}).encode("utf-8"))
    assert not constraint.validate(json.dumps({"name": "Jerry", "age": "abc"}))
