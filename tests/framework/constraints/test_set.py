from datetime import date, datetime, time, timedelta, timezone

import pytest

from sila2.framework import ValidationError
from sila2.framework.data_types.date import Date, SilaDateType
from sila2.framework.data_types.integer import Integer
from sila2.framework.data_types.real import Real
from sila2.framework.data_types.string import String
from sila2.framework.data_types.time import Time
from sila2.framework.data_types.timestamp import Timestamp
from sila2.framework.feature import Feature
from tests.utils import get_feature_definition_str


@pytest.fixture
def set_feature() -> Feature:
    return Feature(get_feature_definition_str("Set"))


def test_string_set(set_feature, silaframework_pb2_module):
    dtype = set_feature._data_type_definitions["StringSet"]
    for s in ["ABC", "", "this is a string"]:
        dtype.to_message(s)
        assert dtype.to_native_type(dtype.to_message(s)) == s
        assert dtype.data_type.to_native_type(String().to_message(s)) == s

    with pytest.raises(ValidationError):
        dtype.to_message("abc")

    with pytest.raises(ValidationError):
        dtype.to_message(" ")


def test_integer_set(set_feature, silaframework_pb2_module):
    dtype = set_feature._data_type_definitions["IntegerSet"]
    for s in [1, 0, -123]:
        dtype.to_message(s)
        assert dtype.to_native_type(dtype.to_message(s)) == s
        assert dtype.data_type.to_native_type(Integer().to_message(s)) == s

    with pytest.raises(ValidationError):
        dtype.to_message(123)

    with pytest.raises(ValidationError):
        dtype.to_message(-1)

    assert repr(dtype.data_type.constraints[0]) == "Set([1, 0, -123])"


def test_real_set(set_feature, silaframework_pb2_module):
    dtype = set_feature._data_type_definitions["RealSet"]
    for s in [1, 0, -0.5, 1 / 3]:
        dtype.to_message(s)
        assert dtype.to_native_type(dtype.to_message(s)) == s
        assert dtype.data_type.to_native_type(Real().to_message(s)) == s

    with pytest.raises(ValidationError):
        dtype.to_message(1.1)

    with pytest.raises(ValidationError):
        dtype.to_message(0.5)


def test_date_set(set_feature, silaframework_pb2_module):
    dtype = set_feature._data_type_definitions["DateSet"]
    for d, tz in [
        (date(2000, 1, 31), timezone(timedelta(0))),
        (date(100, 12, 31), timezone(timedelta(hours=14))),
        (date(2020, 2, 29), timezone(timedelta(hours=-2, minutes=-34))),
    ]:
        dtype.to_message((d, tz))
        assert dtype.to_native_type(dtype.to_message((d, tz))) == (d, tz)
        assert dtype.data_type.to_native_type(Date().to_message(SilaDateType(d, tz))) == (d, tz)

    with pytest.raises(ValidationError):
        dtype.to_message((date(100, 12, 31), timezone(timedelta(hours=-14))))


def test_time_set(set_feature, silaframework_pb2_module):
    dtype = set_feature._data_type_definitions["TimeSet"]
    for s in [
        time(0, tzinfo=timezone(timedelta(0))),
        time(23, 59, 59, tzinfo=timezone(timedelta(hours=14))),
        time(12, 34, 56, tzinfo=timezone(timedelta(hours=-2, minutes=-34))),
    ]:
        dtype.to_message(s)
        assert dtype.to_native_type(dtype.to_message(s)) == s
        assert dtype.data_type.to_native_type(Time().to_message(s)) == s

    with pytest.raises(ValidationError):
        dtype.to_message(time(hour=1, tzinfo=timezone(timedelta(hours=-1))))


def test_timestamp_set(set_feature, silaframework_pb2_module):
    dtype = set_feature._data_type_definitions["TimestampSet"]
    for s in [
        datetime(2000, 1, 31, 0, 0, 0, tzinfo=timezone(timedelta(0))),
        datetime(100, 12, 31, 23, 59, 59, tzinfo=timezone(timedelta(hours=14))),
        datetime(2020, 2, 29, 12, 34, 56, tzinfo=timezone(timedelta(hours=-2, minutes=-34))),
    ]:
        dtype.to_message(s)
        assert dtype.to_native_type(dtype.to_message(s)) == s
        assert dtype.data_type.to_native_type(Timestamp().to_message(s)) == s

    with pytest.raises(ValidationError):
        dtype.to_message(datetime(2000, 1, 31, 1, 0, 0, tzinfo=timezone(timedelta(hours=-1))))
