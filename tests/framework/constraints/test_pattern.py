from sila2.framework.constraints.pattern import Pattern


def test():
    p = Pattern("[0-9]{2}")
    assert p.validate("10")
    assert not p.validate("123")

    assert repr(p) == "Pattern('[0-9]{2}')"
