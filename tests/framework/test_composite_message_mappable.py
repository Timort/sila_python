import pytest

from sila2.framework.errors.validation_error import ValidationError
from sila2.framework.feature import Feature
from tests.utils import get_feature_definition_str


@pytest.fixture
def observable_command_feature():
    return Feature(get_feature_definition_str("ObservableCommand"))


@pytest.fixture
def binary_transfer_feature():
    return Feature(get_feature_definition_str("BinaryTransfer"))


def test(observable_command_feature):
    p = observable_command_feature._observable_commands["NestedTypes"].parameters

    # sanity check
    args = 1, [("abc", True), ("def", False)]
    msg = p.to_message(*args)
    assert p.to_native_type(msg).A == 1
    assert p.to_native_type(msg).B[0].I == "abc"  # noqa: E741
    assert p.to_native_type(msg).B[0].J is True
    assert p.to_native_type(msg).B[1].I == "def"  # noqa: E741
    assert p.to_native_type(msg).B[1].J is False
    assert p.to_native_type(msg) == args

    # only args
    assert p.to_native_type(p.to_message(args)) == args
    assert p.to_native_type(p.to_message(*args)) == args

    # only kwargs
    kwargs = dict(A=1, B=[dict(I="abc", J=True), dict(I="def", J=False)])
    assert p.to_native_type(p.to_message(kwargs)) == args
    assert p.to_native_type(p.to_message(**kwargs)) == args

    # mixed
    assert p.to_native_type(p.to_message(1, B=[("abc", True), ("def", False)])) == args
    assert p.to_native_type(p.to_message(1, B=[dict(I="abc", J=True), dict(I="def", J=False)])) == args
    assert p.to_native_type(p.to_message(1, [dict(I="abc", J=True), ("def", False)])) == args

    # error cases
    with pytest.raises(TypeError):  # multiple values for same key
        _ = p.to_message(1, A=2)
    with pytest.raises(TypeError):  # additional args
        _ = p.to_message(1, [("abc", True), ("def", False)], b"abc")
    with pytest.raises(TypeError):  # additional kwargs
        _ = p.to_message(1, [("abc", True), ("def", False)], C=b"abc")
    with pytest.raises(TypeError):  # wrong kwarg
        _ = p.to_message(A=1, C=b"abc")


def test_unpacking_errors(binary_transfer_feature):
    cmd = binary_transfer_feature._unobservable_commands["TestCommand"]
    params = cmd.parameters
    resps = cmd.responses
    binary_field = binary_transfer_feature._pb2_module.SiLAFramework__pb2.Binary

    # invalid message: Binary requires value or binaryTransferUUID
    msg = params.message_type(Param1=binary_field(), Param2=[])
    with pytest.raises(ValidationError):
        params.to_native_type(msg)

    msg = resps.message_type(Response1=binary_field(), Response2=[])
    with pytest.raises(ValidationError):
        resps.to_native_type(msg)
