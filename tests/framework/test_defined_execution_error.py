import pytest

from sila2.framework.defined_execution_error_node import DefinedExecutionErrorNode
from sila2.framework.feature import Feature
from tests.utils import get_feature_definition_str


@pytest.fixture
def feature() -> Feature:
    return Feature(get_feature_definition_str("ErrorDefinitions"))


def test_string_attributes(feature):
    assert len(feature.defined_execution_errors) == 2
    error1 = feature.defined_execution_errors["Error1"]
    error2 = feature.defined_execution_errors["Error2"]

    assert isinstance(error1, DefinedExecutionErrorNode)
    assert error1._identifier == "Error1"
    assert error1._display_name == "Error 1"
    assert error1._description == "An error"
    assert (
        error1.fully_qualified_identifier == "de.unigoettingen/tests/ErrorDefinitions/v1/DefinedExecutionError/Error1"
    )

    assert isinstance(error2, DefinedExecutionErrorNode)
    assert error2._identifier == "Error2"
    assert error2._display_name == "Error 2"
    assert error2._description == "Another error"
    assert (
        error2.fully_qualified_identifier == "de.unigoettingen/tests/ErrorDefinitions/v1/DefinedExecutionError/Error2"
    )


def test_command_references(feature):
    error1 = feature.defined_execution_errors["Error1"]
    error2 = feature.defined_execution_errors["Error2"]
    command = feature._unobservable_commands["RaiseError"]

    assert error1 in command.defined_execution_errors
    assert error2 in command.defined_execution_errors

    assert len(command.defined_execution_errors) == 2
