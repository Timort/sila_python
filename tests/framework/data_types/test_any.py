from datetime import date, datetime, time, timedelta, timezone

import pytest

from sila2.framework import ValidationError
from sila2.framework.data_types.any import Any, SilaAnyType
from sila2.framework.utils import xml_node_to_normalized_string


@pytest.fixture
def any_field(silaframework_pb2_module) -> Any:
    return Any()


def test_basic(any_field, silaframework_pb2_module):
    msg = any_field.to_message(SilaAnyType("<DataType><Basic>String</Basic></DataType>", "abc"))
    assert isinstance(msg, silaframework_pb2_module.Any)
    assert any_field.to_native_type(msg)[1] == "abc"

    msg = any_field.to_message(SilaAnyType("<DataType><Basic>Integer</Basic></DataType>", 123))
    assert any_field.to_native_type(msg)[1] == 123

    msg = any_field.to_message(SilaAnyType("<DataType><Basic>Real</Basic></DataType>", 1.234))
    assert any_field.to_native_type(msg)[1] == 1.234

    msg = any_field.to_message(SilaAnyType("<DataType><Basic>Binary</Basic></DataType>", b"abc"))
    assert any_field.to_native_type(msg)[1] == b"abc"

    msg = any_field.to_message(
        SilaAnyType("<DataType><Basic>Date</Basic></DataType>", (date(2020, 12, 24), timezone(timedelta(hours=-2))))
    )
    assert any_field.to_native_type(msg)[1] == (date(2020, 12, 24), timezone(timedelta(hours=-2)))

    msg = any_field.to_message(
        SilaAnyType(
            "<DataType><Basic>Time</Basic></DataType>",
            time(13, 59, 26, tzinfo=timezone(timedelta(hours=3, minutes=20))),
        )
    )
    assert any_field.to_native_type(msg)[1] == time(13, 59, 26, tzinfo=timezone(timedelta(hours=3, minutes=20)))

    msg = any_field.to_message(
        SilaAnyType(
            "<DataType><Basic>Timestamp</Basic></DataType>",
            datetime(2020, 12, 24, 13, 59, 26, tzinfo=timezone(timedelta(hours=3, minutes=20))),
        )
    )
    native = any_field.to_native_type(msg)
    assert isinstance(native, SilaAnyType)
    assert native.value == datetime(2020, 12, 24, 13, 59, 26, tzinfo=timezone(timedelta(hours=3, minutes=20)))


def test_constrained_basic(any_field):
    type_str = """<DataType><Constrained>
            <DataType><Basic>Integer</Basic></DataType>
            <Constraints>
                <Set>
                    <Value>1</Value>
                    <Value>2</Value>
                    <Value>3</Value>
                </Set>
            </Constraints>
        </Constrained></DataType>"""

    msg = any_field.to_message(SilaAnyType(type_str, 1))
    assert any_field.to_native_type(msg) == (xml_node_to_normalized_string(type_str), 1)
    msg = any_field.to_message(SilaAnyType(type_str, 2))
    assert any_field.to_native_type(msg) == (xml_node_to_normalized_string(type_str), 2)
    msg = any_field.to_message(SilaAnyType(type_str, 3))
    assert any_field.to_native_type(msg) == (xml_node_to_normalized_string(type_str), 3)

    with pytest.raises(ValidationError):
        any_field.to_message(SilaAnyType(type_str, 4))


def test_list(any_field):
    type_str = """<DataType>
        <List>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </List>
    </DataType>"""

    lst = ["abc", "def", "ghi"]
    msg = any_field.to_message(SilaAnyType(type_str, lst))
    assert any_field.to_native_type(msg) == (xml_node_to_normalized_string(type_str), lst)


def test_structure_with_list(any_field):
    type_str = """<DataType>
            <Structure>
                <Element>
                    <Identifier>Element1</Identifier>
                    <DisplayName>Element1</DisplayName>
                    <Description>First element</Description>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Element2</Identifier>
                    <DisplayName>Element2</DisplayName>
                    <Description>Second element</Description>
                    <DataType>
                        <List>
                            <DataType>
                                <Basic>String</Basic>
                            </DataType>
                        </List>
                    </DataType>
                </Element>
            </Structure>
        </DataType>
    """

    obj = (12, ["ab", "c", "de"])
    msg = any_field.to_message(SilaAnyType(type_str, obj))
    assert any_field.to_native_type(msg) == (xml_node_to_normalized_string(type_str), obj)


def test_structure(any_field):
    type_str = """<DataType>
            <Structure>
                <Element>
                    <Identifier>Element1</Identifier>
                    <DisplayName>Element1</DisplayName>
                    <Description>First element</Description>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Element2</Identifier>
                    <DisplayName>Element2</DisplayName>
                    <Description>Second element</Description>
                    <DataType>
                        <Structure>
                            <Element>
                                <Identifier>Subelement1</Identifier>
                                <DisplayName>Subelement1</DisplayName>
                                <Description>First subelement</Description>
                                <DataType>
                                    <Basic>Boolean</Basic>
                                </DataType>
                            </Element>
                            <Element>
                                <Identifier>Subelement2</Identifier>
                                <DisplayName>Subelement2</DisplayName>
                                <Description>Second subelement</Description>
                                <DataType>
                                    <Basic>Binary</Basic>
                                </DataType>
                            </Element>
                        </Structure>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Element3</Identifier>
                    <DisplayName>Element3</DisplayName>
                    <Description>Third element</Description>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                </Element>
            </Structure>
        </DataType>
    """

    obj = (1, (True, b"abc"), "def")
    msg = any_field.to_message(SilaAnyType(type_str, obj))
    assert any_field.to_native_type(msg) == (xml_node_to_normalized_string(type_str), obj)


def test_void(any_field, silaframework_pb2_module):
    type_str = """
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Length>0</Length>
                </Constraints>
            </Constrained>
        </DataType>"""

    msg = any_field.to_message(SilaAnyType(type_str, ""))
    t, val = any_field.to_native_type(msg)
    assert t == xml_node_to_normalized_string(type_str)
    assert val is None

    msg = any_field.to_message(SilaAnyType(type_str, None))
    assert silaframework_pb2_module.String.FromString(msg.payload).value == ""
    t, val = any_field.to_native_type(msg)
    assert val is None
