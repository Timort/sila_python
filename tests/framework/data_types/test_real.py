import pytest

from sila2.framework.data_types.real import Real
from sila2.framework.pb2 import SiLAFramework_pb2


def test_to_native_type():
    msg = SiLAFramework_pb2.Real(value=10.0)
    val = Real().to_native_type(msg)

    assert isinstance(val, float)
    assert val == 10.0


def test_to_message():
    msg = Real().to_message(10.0)
    assert msg.value == 10.0


def test_wrong_type():
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        Real().to_message("abc")


def test_overflow():
    with pytest.raises(OverflowError):
        Real().to_message(10**1000)


def test_from_string():
    real_field = Real()

    assert real_field.from_string("1") == 1
    assert real_field.from_string("1.") == 1
    assert real_field.from_string("1.0") == 1
    assert real_field.from_string(".5") == 0.5
    assert real_field.from_string("-.5") == -0.5
    assert real_field.from_string("+.5") == 0.5

    with pytest.raises(ValueError):
        real_field.from_string("1.1.1")
