import pytest

from sila2.framework.data_types.string import String
from sila2.framework.pb2 import SiLAFramework_pb2


def test_to_native_type():
    msg = SiLAFramework_pb2.String(value="abc")
    val = String().to_native_type(msg)

    assert isinstance(val, str)
    assert val == "abc"


def test_to_message():
    msg = String().to_message("def")
    assert msg.value == "def"


def test_wrong_type():
    string_field = String()

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        string_field.to_message(b"abc")

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        string_field.to_message(123)


def test_too_long():
    string_field = String()

    longest_allowed_string = "a" * 2**21

    _ = string_field.to_message(longest_allowed_string)
    with pytest.raises(ValueError):
        string_field.to_message(longest_allowed_string + "a")


def test_from_string():
    assert String().from_string("abc") == "abc"
