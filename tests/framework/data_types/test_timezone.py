from datetime import timedelta, timezone, tzinfo

import pytest

from sila2.framework.data_types.timezone import Timezone


def test_to_native_type(basic_feature):
    SilaTimezone = basic_feature._pb2_module.SiLAFramework__pb2.Timezone
    timezone_field = Timezone()

    msg = SilaTimezone(hours=3, minutes=30)
    tz = timezone_field.to_native_type(msg)

    assert isinstance(tz, tzinfo)
    assert tz.utcoffset(None) == timedelta(hours=3, minutes=30)


def test_to_message(basic_feature):
    SilaTimezone = basic_feature._pb2_module.SiLAFramework__pb2.Timezone
    timezone_field = Timezone()

    msg = timezone_field.to_message(timezone(timedelta(hours=3, minutes=30)))

    assert isinstance(msg, SilaTimezone)
    assert msg.hours == 3
    assert msg.minutes == 30


def test_seconds_in_timezone(basic_feature):
    timezone_field = Timezone()

    with pytest.raises(ValueError):
        _ = timezone_field.to_message(timezone(timedelta(hours=3, minutes=30, seconds=1)))


def test_from_string(basic_feature):
    timezone_field = Timezone()

    assert timezone_field.from_string("Z") == timezone(timedelta(0))
    assert timezone_field.from_string("-14:00") == timezone(timedelta(hours=-14))
    assert timezone_field.from_string("+14:00") == timezone(timedelta(hours=14))

    with pytest.raises(ValueError):
        _ = timezone_field.from_string("+14:01")
    with pytest.raises(ValueError):
        _ = timezone_field.from_string("-14:01")
    with pytest.raises(ValueError):
        _ = timezone_field.from_string("-14:01:30")
    with pytest.raises(ValueError):
        _ = timezone_field.from_string("-14:01:30")
