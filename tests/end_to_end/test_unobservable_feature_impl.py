import time
from typing import Tuple

import pytest

from sila2.client.sila_client import SilaClient
from sila2.server.sila_server import SilaServer
from tests.end_to_end.greetingprovider_impl import GreetingProvider, GreetingProviderImpl
from tests.utils import generate_port


@pytest.fixture
def server_client() -> Tuple[SilaServer, SilaClient]:
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

            self.set_feature_implementation(GreetingProvider, GreetingProviderImpl(self))

    port = generate_port()
    address = "127.0.0.1"
    server = TestServer()
    server.start_insecure(address, port, enable_discovery=False)
    client = SilaClient(address, port, insecure=True)

    return server, client


def test(server_client):
    server, client = server_client

    assert client.GreetingProvider.SayHello("World").Greeting == "Hello World"
    assert client.GreetingProvider.StartYear.get() == 2022

    sub = client.GreetingProvider.StartYear.subscribe_by_polling(poll_interval=0.1)
    sub.add_callback(lambda year: None)
    time.sleep(1)
    sub.cancel()
    assert list(sub) == [2022]

    with pytest.raises(RuntimeError):
        sub.add_callback(lambda year: None)

    server.stop()
