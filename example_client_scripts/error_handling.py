from sila2_example_server import Client
from sila2_example_server.generated.delayprovider import DelayTooLong

from sila2.client import SilaClient
from sila2.framework import DefinedExecutionError, SilaConnectionError, ValidationError


def main():
    certificate_authority = open("ca.pem", "rb").read()
    try:
        SilaClient("127.0.0.1", 50052, insecure=True)
    except SilaConnectionError as error:
        print(f"Caught SilaConnectionError: {error}")

    client = Client("127.0.0.1", 50052, root_certs=certificate_authority)

    try:
        client.DelayProvider.RandomNumber.get(metadata=[client.DelayProvider.Delay(100_000)])
    except DelayTooLong as error:  # only works if the error type was registered to the client (non-dynamic)
        print(f"Caught DelayTooLong: {error.message}")

    try:
        client.DelayProvider.RandomNumber.get(metadata=[client.DelayProvider.Delay(100_000)])
    except DefinedExecutionError as error:  # works for all clients
        print(f"Caught DefinedExecutionError {error.identifier}: {error.message}")

    try:
        client.TimerProvider.Countdown(-1, Message="Countdown with negative number")
    except ValidationError as error:
        print(f"Caught ValidationError {error}: {error.message}")


if __name__ == "__main__":
    main()
