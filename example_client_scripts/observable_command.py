import time

from sila2_example_server import Client
from sila2_example_server.generated.timerprovider import CountdownTooLong

from sila2.framework import CommandExecutionStatus


def main():
    certificate_authority = open("ca.pem", "rb").read()
    client = Client("127.0.0.1", 50052, root_certs=certificate_authority)

    # calling an observable command returns a ClientObservableCommandInstance
    print("Starting 5 second countdown")
    countdown_instance = client.TimerProvider.Countdown(N=5, Message="Countdown ended")

    # subscribe to the intermediate responses and print them
    intermediate_response_subscription = countdown_instance.subscribe_to_intermediate_responses()
    intermediate_response_subscription.add_callback(lambda resp: print(f"Current number: {resp.CurrentNumber}"))

    # get current intermediate response value
    print("Current intermediate response:", countdown_instance.get_intermediate_response())

    # print status while countdown is running
    while not countdown_instance.done:
        time.sleep(0.5)
        print(
            f"Status: {countdown_instance.status}, "
            f"Progress: {countdown_instance.progress}, "
            f"Remaining time: {countdown_instance.estimated_remaining_time}"
        )

    # receive responses
    message, timestamp = countdown_instance.get_responses()
    print(f"Responses: {timestamp.isoformat()}: {message}")

    # errors are raised when requesting the result
    print("Starting countdown with invalid duration")
    countdown_instance = client.TimerProvider.Countdown(N=9001, Message="Wake up!")
    time.sleep(0.5)

    assert countdown_instance.status == CommandExecutionStatus.finishedWithError
    try:
        countdown_instance.get_responses()
    except CountdownTooLong:
        print("Countdown duration was invalid and the error was caught")


if __name__ == "__main__":
    main()
