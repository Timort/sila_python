import time

from sila2_example_server import Client


def main():
    certificate_authority = open("ca.pem", "rb").read()
    client = Client("127.0.0.1", 50052, root_certs=certificate_authority)

    # get current value
    current_time = client.TimerProvider.CurrentTime.get()
    print("Current time:", current_time)

    # subscribe to value updates
    time_subscription = client.TimerProvider.CurrentTime.subscribe()

    # print every new value
    print("Print new values:")
    time_subscription.add_callback(print)

    # wait
    time.sleep(3)

    # subscriptions stay active until it is explicitly cancelled:
    time_subscription.cancel()
    print("Cancelled subscription")

    # received values are iterable:
    print("All received values:", list(time_subscription))


if __name__ == "__main__":
    main()
